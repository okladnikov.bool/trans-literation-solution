package com.epam.autocode.transliteration;

public interface MutatorFactory {
    StreamMutator createStreamMutator();
}
