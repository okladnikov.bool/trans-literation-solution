package com.epam.autocode.transliteration;

import java.util.Map;
import java.util.stream.Stream;

/**
 * StreamMutator.java Declaration of any stream mutation
 */
public interface StreamMutator {
    /**
     * Function for mapping symbols of given stream of strings to output stream of strings
     * @param streamIn the stream of strings that gives for processing
     * @param mutationMap the dictionary that should be used for making metamorphosis
     * @return the stream of strings that contains brand-new data
     */
    Stream<String> mutateStringStream(Stream<String> streamIn, Map<String, String> mutationMap);
}
