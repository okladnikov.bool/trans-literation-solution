package com.epam.autocode.transliteration;

import java.util.Map;
import java.util.stream.Stream;

/**
 * SimpleStreamMutator.java is a straightforward implementation of StreamMutator.
 * @see StreamMutator for true identity
 */

public class SimpleStreamMutator implements StreamMutator {

    @Override
    public Stream<String> mutateStringStream(Stream<String> streamIn, Map<String, String> mutationMap) {
        return streamIn.map(s -> {
            StringBuilder result = new StringBuilder();

            for(char symbol: s.toCharArray()){
                if(mutationMap.containsKey(String.valueOf(symbol))) {
                    result.append(mutationMap.get(String.valueOf(symbol)));
                } else {
                    result.append(symbol);
                }
            }

            return result.toString();
        });
    }
}
