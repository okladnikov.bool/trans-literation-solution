package com.epam.autocode.transliteration;

public class SimpleMutatorFactory implements MutatorFactory {

    @Override
    public StreamMutator createStreamMutator() {
        return new SimpleStreamMutator();
    }

}
