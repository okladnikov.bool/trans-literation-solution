package com.epam.autocode.transliteration;

import org.junit.jupiter.api.*;

import java.util.*;
import java.util.stream.Stream;

@DisplayName("Transliteration test")
public class TestTransliteration {
    static Map<String, String> mutationMap = new HashMap<>();
    static MutatorFactory factory;
    static StreamMutator mutator;

    @BeforeAll
    static void initMutation() {
        factory = new SimpleMutatorFactory();

        mutator = factory.createStreamMutator();

        mutationMap.put("А", "A");
        mutationMap.put("а", "a");
        mutationMap.put("Б", "6");
        mutationMap.put("б", "6");
        mutationMap.put("В", "B");
        mutationMap.put("в", "B");
        mutationMap.put("Г", "r");
        mutationMap.put("г", "r");
        mutationMap.put("Д", "g");
        mutationMap.put("д", "g");
        mutationMap.put("Е", "E");
        mutationMap.put("е", "e");
        mutationMap.put("Ё", "E");
        mutationMap.put("ё", "e");
        mutationMap.put("Ж", "}|{");
        mutationMap.put("ж", "}|{");
        mutationMap.put("З", "3");
        mutationMap.put("з", "3");
        mutationMap.put("И", "|/|");
        mutationMap.put("и", "u");
        mutationMap.put("Й", "|/|");
        mutationMap.put("й", "u");
        mutationMap.put("К", "K");
        mutationMap.put("к", "K");
        mutationMap.put("Л", "Jl");
        mutationMap.put("л", "Jl");
        mutationMap.put("М", "M");
        mutationMap.put("м", "M");
        mutationMap.put("Н", "H");
        mutationMap.put("н", "H");
        mutationMap.put("О", "O");
        mutationMap.put("о", "o");
        mutationMap.put("П", "n");
        mutationMap.put("п", "n");
        mutationMap.put("Р", "P");
        mutationMap.put("р", "p");
        mutationMap.put("С", "C");
        mutationMap.put("с", "c");
        mutationMap.put("Т", "T");
        mutationMap.put("т", "T");
        mutationMap.put("У", "y");
        mutationMap.put("у", "y");
        mutationMap.put("Ф", "(|)");
        mutationMap.put("ф", "qp");
        mutationMap.put("Х", "X");
        mutationMap.put("х", "x");
        mutationMap.put("Ц", "|/|");
        mutationMap.put("ц", "u");
        mutationMap.put("Ч", "4");
        mutationMap.put("ч", "4");
        mutationMap.put("Ш", "LLl");
        mutationMap.put("ш", "LLl");
        mutationMap.put("Щ", "LLL");
        mutationMap.put("щ", "LLL");
        mutationMap.put("Ъ", "b");
        mutationMap.put("ъ", "b");
        mutationMap.put("Ы", "bl");
        mutationMap.put("ы", "bl");
        mutationMap.put("Ь", "b");
        mutationMap.put("ь", "b");
        mutationMap.put("Э", "3");
        mutationMap.put("э", "3");
        mutationMap.put("Ю", "|-O");
        mutationMap.put("ю", "l-o");
        mutationMap.put("Я", "9l");
        mutationMap.put("я", "9l");
    }

    @Test
    @DisplayName("Standard test")
    void standardTest() {
        Stream<String> phrasesStream = Arrays.stream(new String[] {
                "кот в кедах",
                "Смотрю мультик \"Тачки 2\"",
                "КОСМОНАВТ",
                "СВЕКРОВЬ",
                "хохма",
                "ИВАН"
        });

        List<String> expectedMutatedPhrases = Arrays.asList("KoT B Kegax",
                "CMoTpl-o MyJlbTuK \"Ta4Ku 2\"",
                "KOCMOHABT",
                "CBEKPOBb",
                "xoxMa",
                "|/|BAH");
        List<String> actualMutatedPhrases = new ArrayList<>();

        mutator.mutateStringStream(phrasesStream, mutationMap)
                .forEach(actualMutatedPhrases::add);

        Assertions.assertEquals(expectedMutatedPhrases, actualMutatedPhrases);
    }

    @Test
    @DisplayName("Empty input stream test")
    void emptyStreamTest() {
        Stream<String> phrasesStream = Arrays.stream(new String[] {});

        List<String> expectedMutatedPhrases = Arrays.asList();
        List<String> actualMutatedPhrases = new ArrayList<>();

        mutator.mutateStringStream(phrasesStream, mutationMap)
                .forEach(actualMutatedPhrases::add);

        Assertions.assertEquals(expectedMutatedPhrases, actualMutatedPhrases);
    }

    @Test
    @DisplayName("No Cyrillic data test")
    void noCyrillicTest() {
        Stream<String> phrasesStream = Arrays.stream(new String[] {
                "Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.",
        });

        List<String> expectedMutatedPhrases = Arrays.asList("Sed ut perspiciatis, unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam eaque ipsa, quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam voluptatem, quia voluptas sit, aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos, qui ratione voluptatem sequi nesciunt, neque porro quisquam est, qui dolorem ipsum, quia dolor sit, amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt, ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit, qui in ea voluptate velit esse, quam nihil molestiae consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.");
        List<String> actualMutatedPhrases = new ArrayList<>();

        mutator.mutateStringStream(phrasesStream, mutationMap)
                .forEach(actualMutatedPhrases::add);

        Assertions.assertEquals(expectedMutatedPhrases, actualMutatedPhrases);
    }

    @Test
    @DisplayName("Test whole Cyrillic symbols")
    void wholeAlphabetTest() {
        Stream<String> alphabetStream = Arrays.stream(new String[] {
                "А", "а", "Б", "б", "В", "в", "Г", "г",
                "Д", "д", "Е", "е", "Ё", "ё", "Ж", "ж",
                "З", "з", "И", "и", "Й", "й", "К", "к",
                "Л", "л", "М", "м", "Н", "н", "О", "о",
                "П", "п", "Р", "р", "С", "с", "Т", "т",
                "У", "у", "Ф", "ф", "Х", "х", "Ц", "ц",
                "Ч", "ч", "Ш", "ш", "Щ", "щ", "Ъ", "ъ",
                "Ы", "ы", "Ь", "ь", "Э", "э", "Ю", "ю",
                "Я", "я"
        });

        List<String> expectedMutatedPhrases = Arrays.asList(
                "A", "a", "6", "6", "B", "B", "r", "r",
                "g", "g", "E", "e", "E", "e", "}|{", "}|{",
                "3", "3", "|/|", "u", "|/|", "u", "K", "K",
                "Jl", "Jl", "M", "M", "H", "H", "O", "o",
                "n", "n", "P", "p", "C", "c", "T", "T",
                "y", "y", "(|)", "qp", "X", "x", "|/|", "u",
                "4", "4", "LLl", "LLl", "LLL", "LLL", "b", "b",
                "bl", "bl", "b", "b", "3", "3", "|-O", "l-o",
                "9l", "9l" );
        List<String> actualMutatedPhrases = new ArrayList<>();

        mutator.mutateStringStream(alphabetStream, mutationMap)
                .forEach(actualMutatedPhrases::add);

        Assertions.assertEquals(expectedMutatedPhrases, actualMutatedPhrases);
    }
}
