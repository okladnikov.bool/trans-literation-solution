# Ridiculous transliteration

Given not null stream of strings that may contain Cyrillic symbols and any other, including quotes, space characters, etc. Also given dictionary with string keys and string values.

Implement method **mutateStringStream** of [SimpleStreamMutator](https://gitlab.com/okladnikov.bool/trans-literation-skeleton/-/blob/master/src/main/java/com/epam/autocode/transliteration/SimpleStreamMutator.java):
    
Method takes input stream of strings and dictionary and composes output stream of strings by applying dictionary to input stream.

***

## Examples

```
кот в кедах -> KoT B Kegax
Смотрю мультик "Тачки 2" -> CMoTp|-o MyJlTuK "Ta4Ku 2" 
[КОСМОНАВТ, СВЕКРОВЬ] -> [KOCMOHABT, CBEKPOBb]
хохма -> xoxMa
ИВАН -> |/|BAH
```
